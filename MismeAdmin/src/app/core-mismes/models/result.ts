export interface Result {
    id: number;
    text: string;
    textEN: string;
    textIT: string;
    codeName: string;
    conceptName: string;
}