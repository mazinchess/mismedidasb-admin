
export interface GeneralContent {
    id: number;
    content: string;
    contentEN: string;
    contentIT: string;
    contentTypeId: number;
    contentType: string;
}
