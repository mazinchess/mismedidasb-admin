import { BasicSerieStat } from './basic-stats-serie';

export interface EatStatSerie {
    name: string;
    series: BasicSerieStat[];
}
