
export interface Reminder {
    id: number;
    title: string;
    titleEN: string;
    titleIT: string;
    body: string;
    bodyEN: string;
    bodyIT: string;
    cronExpression: string;
    codeName: string;
}