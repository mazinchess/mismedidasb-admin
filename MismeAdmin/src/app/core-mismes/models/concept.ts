
export interface Concept {
    id: number;
    title: string;
    description: string;
    instructions: string;
    titleEN: string;
    descriptionEN: string;
    instructionsEN: string;
    titleIT: string;
    descriptionIT: string;
    instructionsIT: string;
    image: string;
    createdAt: Date;
    modifiedAt: Date;
}
