export const Constants = {
  SIGNIN_URL: 'account/login',
  CREDENTIALS_KEY: 'credentials',
  GET_USERS: 'user',
  GET_USERS_STATS: 'user/stats',
  GET_USERS_STATS_BY_DATE: 'user/stats-by-date',
  GET_EATS_STATS_BY_DATE: 'user/eats-by-date',
  POLL_BASE: 'poll',
  COMPOUND_DISH_BASE: 'compound-dish',
  DISH_BASE: 'dish',
  EATS_BASE: 'eat',
  TIP_BASE: 'tip',
  REMINDER_BASE: 'reminder',
  RESULT_BASE: 'result',
  ANSWER_BASE: 'answer',
  CONCEPT_BASE: 'concept',
  QUESTION_BASE: 'question',
  CONTACT_US_BASE: 'contact-us',
  GENERAL_CONTENT_BASE: 'general-content',
  GET_PERSONAL_DATAS: 'personal-data',
  GET_TAGS: 'tag',
  GET_ADMIN_USER: 'admin/user',
  UPDATE_DISH: 'dish/update',
  DELETE_DISH: 'dish/delete',
  CHANGE_PASS: 'account/change-password',
  GET_PROFILE: 'account/profile',
  GET_EATS_COUNT: 'user/eat-count'
};
