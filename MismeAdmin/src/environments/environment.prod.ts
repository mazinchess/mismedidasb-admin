/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  // serverUrl: 'http://ec2-34-244-181-197.eu-west-1.compute.amazonaws.com/api/',
  // hubUrl: 'http://ec2-34-244-181-197.eu-west-1.compute.amazonaws.com/'
  // serverUrl: 'http://ec2-34-244-181-197.eu-west-1.compute.amazonaws.com:8082/api/',
  // hubUrl: 'http://ec2-34-244-181-197.eu-west-1.compute.amazonaws.com:8082/'
  // serverUrl: 'https://api-dev.metriri.com/api/',
  // hubUrl: 'https://api-dev.metriri.com/'
  serverUrl: 'https://api.metriri.com/api/',
  hubUrl: 'https://api.metriri.com/'
};
